import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_challenge/login/login_page.dart';

class SplashPage extends StatefulWidget {
  static const routeName = "/";
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  startSplashScreen() async {
    var duration = const Duration(seconds: 2);
    return Timer(duration, () {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => LoginPage()));
      // goToLogin(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
            Image.asset('assets/header-splash.png'),
            Image.asset('assets/logo.png'),
            Image.asset('assets/footer-splash.png')
          ])),
    );
  }

  @override
  void initState() {
    super.initState();
    startSplashScreen();
  }
}
